#!/bin/bash

# Instalando Docker y Docker-Compose

curl -sSL https://get.docker.com/ | sh

sleep 5
sudo usermod -a -G docker ubuntu
sudo systemctl start docker
sudo systemctl enable docker

sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && sudo chmod +x /usr/local/bin/docker-compose \
